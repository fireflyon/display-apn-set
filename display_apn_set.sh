#!/usr/bin/env bash

# Set default params.
device=''
step=1

function set_apn_for_iot_sim_card() {
  numeric=$(adb shell getprop gsm.sim.operator.numeric)
  numeric_ref="310260"
  if [[ "$numeric" -eq $numeric_ref ]]; then
    adb shell 'su 0 content insert --uri content://telephony/carriers --bind name:s:"iot.tmowholesale" --bind current:i:1 --bind numeric:s:"310260" --bind mcc:i:310 --bind mnc:i:260 --bind apn:s:"iot.tmowholesale"'
  else
    adb shell 'su 0 content insert --uri content://telephony/carriers --bind name:s:"iot.tmowholesale" --bind current:i:1 --bind numeric:s:"310240" --bind mcc:i:310 --bind mnc:i:240 --bind apn:s:"iot.tmowholesale"'
  fi

  iot_apn=$(adb shell su 0 content query --uri content://telephony/carriers | grep "name=iot.tmowholesale" | grep -Eio "(_id=)[0-9]{4}" | grep -Eio "[0-9].*")
  adb shell su 0 content insert --uri content://telephony/carriers/preferapn --bind apn_id:i:${iot_apn}
}

function check_connection() {
  attempts=1
  regState=""
  connState=""

  while [[ "$regState" != "REGISTERED" ]] || [[ "$connState" != "CONNECTED" ]]
  do
  if [[ $attempts == 6 ]]; then
    break
  fi

  echo "\nChecking connection state. Attempt: $attempts\n"
  if [[ $(adb shell dumpsys telephony.registry | grep "mServiceState" | grep -o "mDataRegState=0(IN_SERVICE)") ]]; then
    regState="REGISTERED"
  else
    regState="NOT REGISTERED!!!"
  fi

  echo "- $regState"
  wait
  sleep 2

  if [[ $(adb shell dumpsys telephony.registry | grep "mDataConnectionState" | grep -o "[0-9]") -ne "0" ]]; then
    connState="CONNECTED"
  else
    connState="NOT CONNECTED!!!"
  fi

  echo "- $connState"
  ((attempts=attempts+1))
  wait
  sleep 3
  done

  if [[ "$regState" == "REGISTERED" ]] && [[ "$connState" == "CONNECTED" ]]; then
    echo "\n------- COMPLETED -------"
  else
    echo "\n------- RETRY AGAIN -------"
  fi

  sleep 5
}

# Display the Menu
function show_menu() {
  clear
  echo " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ "
  echo " APN EXECUTOR V 1.0 ON THE BITBUCKET"
  echo " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ "
  echo " 0. Exit"
  echo " 1. Configure APN"
  #echo " 2. Test Connection"
}

# Retrieve the user chosen option
function read_options() {
  local choice
  step=1
  read -p "Enter choice [ 0 - 1 ] " choice
  case $choice in
  0) exit 0 ;;
  1)
    set_device_id
    set_apn_for_iot_sim_card
    check_connection
    ;;
  2)
    check_connection
    ;;
  *) echo -e "${RED}Error invalid Option." ;;
  esac
}

function set_device_id() {
  devices=$(adb devices -l | grep 'transport_id' | awk '{print $NF}')
  echo Start >output.txt

  for d in $devices; do
    {
      prefix="transport_id:"
      device=${d#$prefix}
      output=$(adb shell uname -m | awk '{print $1;}')
    } &
  done
}

# Pause the script
function pause() {
  read -p "Press [Enter] key to continue..."
}

# Add all that is needed
function update() {
  git reset --hard origin/master
  git pull origin --no-edit
  exit 0
}

# Create Loop
function startLoop() {
  #git reset --hard origin/master
  #git pull origin --no-edit

  while true; do
    show_menu
    read_options
  done
}

startLoop
