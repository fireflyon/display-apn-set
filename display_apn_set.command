#!/bin/bash

# Usage:
#	 Display APN Executor
#

cd "$(dirname "$0")"

# Call the shell script, giving it permission if needed.
chmod u+x ./display_apn_set.sh
./display_apn_set.sh
